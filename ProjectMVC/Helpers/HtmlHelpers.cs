﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ProjectMVC.Helpers
{
    public static class HtmlHelpers
    {
        public static string Truncate(this HtmlHelper helper, String input, int length)
        {
            if(!String.IsNullOrEmpty(input))
            { 
                if(input.Length<=length)
                {
                    return input;
                }
                else
                {
                    return input.Substring(0, length) + "...";
                }
            }
            return "";
        }
    }
}