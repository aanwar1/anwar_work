﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectMVC.Models;

namespace ProjectMVC.Controllers
{
    public class CourseInstructorController : Controller
    {
        private SchoolContext db = new SchoolContext();

        // GET: CourseInstructor
        public ActionResult Index()
        {
            var courseInstructors = db.CourseInstructors.Include(c => c.course).Include(c => c.instructor);
            return View(courseInstructors.ToList());
        }

        // GET: CourseInstructor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseInstructor courseInstructor = db.CourseInstructors.Find(id);
            if (courseInstructor == null)
            {
                return HttpNotFound();
            }
            return View(courseInstructor);
        }

        // GET: CourseInstructor/Create
        public ActionResult Create()
        {
            ViewBag.CourseId = new SelectList(db.course, "CourseId", "CourseName");
            ViewBag.InstructorId = new SelectList(db.Instructor, "InstructorId", "LastName");
            return View();
        }

        // POST: CourseInstructor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,CourseId,InstructorId")] CourseInstructor courseInstructor)
        {
            if (ModelState.IsValid)
            {
                db.CourseInstructors.Add(courseInstructor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CourseId = new SelectList(db.course, "CourseId", "CourseName", courseInstructor.CourseId);
            ViewBag.InstructorId = new SelectList(db.Instructor, "InstructorId", "LastName", courseInstructor.InstructorId);
            return View(courseInstructor);
        }

        // GET: CourseInstructor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseInstructor courseInstructor = db.CourseInstructors.Find(id);
            if (courseInstructor == null)
            {
                return HttpNotFound();
            }
            ViewBag.CourseId = new SelectList(db.course, "CourseId", "CourseName", courseInstructor.CourseId);
            ViewBag.InstructorId = new SelectList(db.Instructor, "InstructorId", "LastName", courseInstructor.InstructorId);
            return View(courseInstructor);
        }

        // POST: CourseInstructor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CourseId,InstructorId")] CourseInstructor courseInstructor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(courseInstructor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CourseId = new SelectList(db.course, "CourseId", "CourseName", courseInstructor.CourseId);
            ViewBag.InstructorId = new SelectList(db.Instructor, "InstructorId", "LastName", courseInstructor.InstructorId);
            return View(courseInstructor);
        }

        // GET: CourseInstructor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CourseInstructor courseInstructor = db.CourseInstructors.Find(id);
            if (courseInstructor == null)
            {
                return HttpNotFound();
            }
            return View(courseInstructor);
        }

        // POST: CourseInstructor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CourseInstructor courseInstructor = db.CourseInstructors.Find(id);
            db.CourseInstructors.Remove(courseInstructor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
