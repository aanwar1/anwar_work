﻿using ProjectMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectMVC.Controllers
{
    public class XyzController : Controller
    {
        private SchoolContext db = new SchoolContext();
        // GET: Xyz
        public ActionResult Abc()
        {
            //This statement means that fetch everything from the student class which in turn connected to the table and 
            //convert it to a list and put it in var students.
            var students = db.student.ToList();
            return View(students);
            
            //Course math = new Course();
            //math.CourseName = "Maths";
            //math.TotalCredits = 4;

            //Student alex = new Student();
            //alex.firstName = "Alex";
            //alex.lastName = "Rod";

            //Student lynda = new Student();
            //lynda.firstName = "Lynda";
            //lynda.lastName = "Berry";

            //Student jhon = new Student();
            //jhon.firstName = "jhon";
            //jhon.lastName = "Doe";

            //List<Student> students = new List<Student>();
            //students.Add(alex);
            //students.Add(lynda);
            //students.Add(jhon);

            //CourseStudents obj = new CourseStudents();
            //obj.course = math;
            //obj.students = students;

            //Language Integrated Query

            //return View(obj);


        }
        public ActionResult Index()
        {
            return View();
        }
    }
}