﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjectMVC.Models;
using System.IO;
using log4net;

namespace ProjectMVC.Controllers
{
    public class StudentController : Controller
    {
        private SchoolContext db = new SchoolContext();

        //creating a instance of log
        ILog logger = LogManager.GetLogger(typeof(StudentController));

        public ActionResult Search(String SearchBox)
        {
            //select * from db.Student s where s.lastName = SearchBox
            //linq to SQL statements
            var Students = (from s in db.student
                           where s.LastName.Contains(SearchBox) || s.FirstName.Contains(SearchBox)
                           select s).ToList();
            return View("Index",Students); //denotes call the index page based on the student object.
        }



        // GET: Student
        public ActionResult Index()
        {
            logger.Info("enter student index");
            //linq to entities statement
            return View(db.student.ToList());
        }

        // GET: Student/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                logger.Error("no id");
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.student.Find(id);
            if (student == null)
            {
                logger.Error("no id");
                return HttpNotFound();
            }
            logger.Debug("return student");
            return View(student);
        }

        // GET: Student/Create. Whenever we click on a link it will call the get method
        //No values passed it is just to get the form.
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create. Whenever we click on button it will call the HttpPost annotation.
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StudentId,firstName,lastName,EnrollmentDate")] Student student, HttpPostedFileBase ImageFile)//Bind(Include= ) optional
        {
            if (ModelState.IsValid) //check if all the values are valid.
            {
                if(ImageFile != null) //Whatever image is uploaded is not null
                {
                    //
                    string pic = System.IO.Path.GetFileName(ImageFile.FileName); //first get the file name
                    string path = System.IO.Path.Combine(Server.MapPath("~/images/profiles"), pic); //Create the path

                    //file is uploaded
                    ImageFile.SaveAs(path);

                    student.ImagePath = pic; //which pic is associated with which student

                    //Save the image path to the database or send image directly to the database.
                    //in case if we want to store byte[] ie for DB.
                    using(MemoryStream ms = new MemoryStream())
                    {
                        ImageFile.InputStream.CopyTo(ms);
                        student.Picture = ms.GetBuffer();
                    }
                }

                db.student.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index"); //redirect to the index action method in student controller which in turn fetch all the student values and return a view.
            }

            return View(student);
        }

        // GET: Student/Edit/5
        //if Edit needs more parameter it can be passed like public ActionResult Edit(int? id, String abc)
        public ActionResult Edit(int? id) 
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.student.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StudentId,firstName,lastName,EnrollmentDate")] Student student)
        {
            if (ModelState.IsValid)
            {
                //This line tells that the student object is not a new object. Existing object has been modified
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.student.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Student/Delete/5
        [HttpPost, ActionName("Delete")] 
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) //no two same methods receive one Id.
        {
            Student student = db.student.Find(id);
            db.student.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //to display a image
        public ActionResult GetImage(int id)
        {
            //find the particular picture for a student by id and returns a particular image
            byte[] imagedata = db.student.Find(id).Picture;
            return File(imagedata, "image/jpeg"); //depends on the type of image
        }
    }
}
