CREATE TABLE [dbo].[Log4net] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Date]      DATETIME       NOT NULL,
    [Thread]    VARCHAR (255)  NOT NULL,
    [Level]     VARCHAR (10)   NOT NULL,
    [Logger]    VARCHAR (1000) NOT NULL,
    [Message]   VARCHAR (4000) NOT NULL,
    [Exception] VARCHAR (4000) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)