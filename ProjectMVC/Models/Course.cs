﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class Course
    {
        //To make it non auto generated we provide the annotation
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourseId { get; set; }

        [Required(ErrorMessage="Title is Required")]
        [MaxLength(50)]
        [Display(Name="Title")]
        public String CourseName { get; set; }

        [Required(ErrorMessage="Number of credits is required")]
        [Range(0,5, ErrorMessage="Number of credits must be between 0 and 5")]
        [Display(Name="Credits")]
        public int TotalCredits { get; set; }

        [Display(Name="Department")]
        public int DepartmentId { get; set; }


        //Every course has multiple enrollments.
        public ICollection<Enrollment> Enrollments { get; set; }
        public virtual Department Department { get; set; }

        //Every course can be taught by multiple instructor
        //Since after adding a thrid mapping class CourseInstructor there is no direct relationship bw course and instructor
        //public virtual ICollection<Instructor> Instructor { get; set; }
        public virtual ICollection<CourseInstructor> CourseInstructor { get; set; }
    }
}