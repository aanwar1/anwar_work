﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class Enrollment
    {
        public int EnrollmentId { get; set; }
        public int CourseId { get; set; }

        public int StudentId { get; set; }

        //No question mark Grade will be 0.0, ques mark means accept null value.
        [DisplayFormat(DataFormatString="{0:#.#}",ApplyFormatInEditMode=true, NullDisplayText="No Grade", HtmlEncode=true)]
        public decimal? Grade { get; set; }

        //Every enrollment will have one student and one course object.
        public virtual Student student { get; set; }

        public virtual Course course{get; set;}
    }
}