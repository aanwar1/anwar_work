﻿using ProjectMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class SchoolInitializer : DropCreateDatabaseIfModelChanges<SchoolContext>
    {
        protected override void Seed(SchoolContext context)
        {
                var instructor = new List<Instructor>
                {
                    new Instructor{FirstName = "Peter", LastName="Lee", HireDate=DateTime.Parse("2013-04-10")},
                    new Instructor{FirstName = "Ruth", LastName="Zen", HireDate=DateTime.Parse("2014-08-10")}
                };
                foreach(var temp in instructor)
                {
                    context.Instructor.Add(temp);
                }
                context.SaveChanges();

                var students = new List<Student>
                {
                    new Student{FirstName="James", LastName="Dean",EnrollmentDate=DateTime.Parse("2014-01-02")},
                    new Student{FirstName="Lynda", LastName="Thames",EnrollmentDate=DateTime.Parse("2014-11-02")}
                };

                foreach (var temp in students)
                {
                    context.student.Add(temp);
                }
                context.SaveChanges();


                var department = new List<Department>
                {
                    new Department {Name="English", Budget=200000, StartDate=DateTime.Parse("2012-09-01"), InstructorId=1},
                    new Department {Name="CS & IT", Budget=200000, StartDate=DateTime.Parse("2012-11-01"), InstructorId=2}
                };

                foreach(var temp in department)
                {
                    context.Department.Add(temp);
                }
                context.SaveChanges();

                /*var courses = new List<Course>
                {
                    new Course{CourseId=100, CourseName="Java", TotalCredits=4, DepartmentId=2, Instructor=new  List<Instructor>()},
                    new Course{CourseId=200, CourseName="C#", TotalCredits=4, DepartmentId=2,Instructor=new  List<Instructor>()}
                };

                foreach (var temp in courses)
                {
                    context.course.Add(temp);
                }
                context.SaveChanges();

                courses[0].Instructor.Add(instructor[0]);
                courses[0].Instructor.Add(instructor[1]);
                context.SaveChanges();*/

                //Since we made a thrid mapping class CourseInstructor above code is not needed.
                var courses = new List<Course>
                {
                    new Course{CourseId=100, CourseName="Java", TotalCredits=4, DepartmentId=2},
                    new Course{CourseId=200, CourseName="C#", TotalCredits=4, DepartmentId=2}
                };

                foreach (var temp in courses)
                {
                    context.course.Add(temp);
                }
                context.SaveChanges();

                var enrollment = new List<Enrollment>
                 {
                    new Enrollment{StudentId=1, CourseId=100,Grade=3},
                    new Enrollment{StudentId=1, CourseId=200,Grade=4},
                };

                /* var enrollment = new List<Enrollment>
                  {
                     new Enrollment{PersonId=1, CourseId=100,Grade=3},
                     new Enrollment{PersonId=1, CourseId=200,Grade=4},
                 };*/

                foreach (var temp in enrollment)
                {
                    context.Enrollment.Add(temp);
                }
                context.SaveChanges();

                /*var courseinstructor = new List<CourseInstructor>
                {
                    new CourseInstructor {PersonId=1,CourseId=100},
                    new CourseInstructor {PersonId=2,CourseId=100}

                };*/
                var courseinstructor = new List<CourseInstructor>
                {
                    new CourseInstructor {InstructorId=1,CourseId=100},
                    new CourseInstructor {InstructorId=2,CourseId=100}

                };
                foreach (var temp in courseinstructor)
                {
                    context.CourseInstructors.Add(temp);
                }
                context.SaveChanges();
            }
        }
    }