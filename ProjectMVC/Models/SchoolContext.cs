﻿using ProjectMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class SchoolContext:DbContext
    {
        //enables crud functionality
        public DbSet<Student> student { get; set; }
        public DbSet<Course> course { get; set; }
        
        public DbSet<Enrollment> Enrollment { get; set; }

        public DbSet<Instructor> Instructor { get; set; }
        
        public DbSet<Department> Department { get; set; }

        //public DbSet<Person> People { get; set; }

        public DbSet<CourseInstructor> CourseInstructors { get; set; }

        /*protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //fluent API to control the creation of 3rd table of InstructorCourse of mapping table. A way to create the third table
            modelBuilder.Entity<Course>() //Entity course has many instructors and this instructors inturn has many courses
                .HasMany(c => c.Instructor).WithMany(i => i.Courses) 
                .Map(t => t.MapLeftKey("CourseId") //Map from left Course table the courseId column and right i.e instructor table
                .MapRightKey("PersonId") //PersonId column
                .ToTable("InstructorCourse")); //to a table called InstructorCourse
        }*/

    }
}