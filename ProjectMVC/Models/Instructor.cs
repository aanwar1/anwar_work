﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class Instructor 
    {
        //Since we are inheriting from person which already has Id, FirstName and LastName.
         public int InstructorId { get; set; }

         [Required(ErrorMessage="Last name is required")]
         [Display(Name="Last Name")]
         [MaxLength(50)]
         public string LastName { get; set; }

         [Required(ErrorMessage = "First name is required")]
         [Display(Name = "First Name")]
         [MaxLength(50)]
         public string FirstName { get; set; } 

        [Required(ErrorMessage = "Hire date is required")]
        [Display(Name = "Hire Date")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime? HireDate { get; set; }


        [Display(Name = "Instructor Name")]
        [NotMapped] //If we don't want the column to be created
        public string FullName
        {
            get
            {
                return LastName + " , " + FirstName;
            }
        }

        //every instructor can teach a list of courses.
        //Same comment as Course.cs
        //public virtual ICollection<Course> Courses { get; set; }
         public virtual ICollection<CourseInstructor> CourseInstructor { get; set; }
    }
}