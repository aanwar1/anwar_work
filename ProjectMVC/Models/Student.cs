﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjectMVC.Models
{
    public class Student
    {
        //Since we are inheriting from person which already has Id, FirstName and LastName.
        [Key]
        public int StudentId { get; set; }

        [Required (ErrorMessage="First name is required")]
        [MaxLength(50)]
        [Display(Name = "First Name")]
        public String FirstName { get; set; }

        [Required(ErrorMessage = "Family name is required")]
        [MaxLength(50)]
        [Display (Name="Family Name")]
        public String LastName { get; set; }

        [Required(ErrorMessage = "Enrollment date is required")]
        [Display(Name = "Enrollment Date")]
        [DisplayFormat(DataFormatString="{0:d}",ApplyFormatInEditMode=true)]
        public DateTime EnrollmentDate { get; set; }

        //implementing picture storing of students
        [Column(TypeName="image")]
        public byte[] Picture { get; set; }

        public string ImagePath { get; set; }
        //represents every student is going to have multiple enrollments.
        //public ICollection<Enrollment> Enrollments { get; set; }
        //When we put virtual key word a concept called lazy loading takes place.
        public virtual ICollection<Enrollment> Enrollments { get; set; }

    }
}